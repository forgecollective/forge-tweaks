=== Forge Tweaks ===
Contributors: samjdavis
Tags: Forge Collective, White Label
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Requires at least: 3.0.1
Tested up to: 4.5.3
Stable tag: trunk

A simple plugin to white label sites created by Forge Collective.

== Description ==

A simple plugin to white label sites created by Forge Collective.

* Adds a custom login screen
* Customises the footer text in the WordPress admin.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress

== Screenshots ==

1. Login Screen Example

== Changelog ==

= 2.1.0 =
* Completely rewrite from the ground up